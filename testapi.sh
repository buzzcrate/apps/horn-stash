#!/bin/bash

PORT=8900
URL="http://localhost"
ROUTE=""

# Get entire keyvalue store
curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}

# Create new entry in keyvalue store
curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData", "Value": "12345Blah"}}' \
    ${URL}:${PORT}/${ROUTE}

# Get entire keyvalue store
curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}

# Change already listed entry in keyvalue store
curl -i \
    -X PUT \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData2", "Value": "ZARF!", "Other": "Adding field", "Nested":{"nested1": "working"}}}' \
    ${URL}:${PORT}/${ROUTE}

# Get entire keyvalue store
curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}
    
# Create second entry in keyvalue store
curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"otherTest":{"Name": "TestData", "Value": "12345Blah"}}' \
    ${URL}:${PORT}/${ROUTE}

# Get specific value from keyvalue store
curl -i \
    -X GET \
    --data "otherTest" \
    ${URL}:${PORT}/${ROUTE}get

# Get entire keyvalue store
curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}

# Remove specific entry of keyvalue store
curl -i \
    -X DELETE \
    -H "Content-Type: application/json" \
    --data '"localtest"' \
    ${URL}:${PORT}/${ROUTE}

# Get entire keyvalue store
curl -i \
    -X GET \
    -H "Content-Type: application/json" \
    ${URL}:${PORT}/${ROUTE}
