#!/bin/bash

PORT=8900
URL="http://localhost"
ROUTE=""


curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData", "Value": "12345Blah"}}' \
    ${URL}:${PORT}/${ROUTE}


curl -i \
    -X PUT \
    -H "Content-Type: application/json" \
    --data '{"localtest":{"Name": "TestData2", "Value": "ZARF!"}}' \
    ${URL}:${PORT}/${ROUTE}


curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"taco":{"Name": "tacodata", "Value": "credit card"}}' \
    ${URL}:${PORT}/${ROUTE}


curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"nacho":{"Name": "Cheesy Data", "Value": "Not your Cheese"}}' \
    ${URL}:${PORT}/${ROUTE}


curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"unicorn":{"Name": "defense", "Value": "Unicorns fart"}}' \
    ${URL}:${PORT}/${ROUTE}


curl -i \
    -X POST \
    -H "Content-Type: application/json" \
    --data '{"patch":{"Name": "sacred taco", "Value": "toot in enchanted forest"}}' \
    ${URL}:${PORT}/${ROUTE}

